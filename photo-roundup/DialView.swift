//
//  DialView.swift
//  Hydrade
//
//  Created by Lee Irvine on 6/25/18.
//  Copyright © 2018 kezzi.co. All rights reserved.
//

import UIKit

protocol DialViewDelegate: NSObjectProtocol {
    func didChangeProgress(_:DialView)
}

class DialView: UIView {
    @IBInspectable
    var progress:Double = 0 { // number 0 to 1
        didSet {
            refresh()
        }
    }

    @IBInspectable
    var interactive: Bool = false {
        didSet {
            self.isUserInteractionEnabled = interactive
            refresh()
        }
    }

    @IBInspectable
    var emptyStrokeSize: CGFloat = 2.0

    @IBInspectable
    var fillStrokeSize: CGFloat = 6.0

    @IBInspectable
    var dialFillColor: UIColor = UIColor.blue

    @IBInspectable
    var dialEmptyColor: UIColor = UIColor.clear

    @IBInspectable
    var dialBackgroundColor: UIColor = UIColor.clear

    var delegate: DialViewDelegate?

    private var nubView: NubView!

    private var interactGesture: UIPanGestureRecognizer!

    private func refresh() {
        self.setNeedsDisplay()

        if let nubView = self.nubView {
            nubView.isHidden = !interactive
            nubView.frame = CGRect(origin: nubOrigin(), size: nubSize())
        }
    }

    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = false

        self.nubView = NubView(frame: CGRect(origin: self.nubOrigin(), size: self.nubSize()))
        self.addSubview(self.nubView)
        self.nubView.isHidden = !interactive

        self.interactGesture = UIPanGestureRecognizer(target: self, action: #selector(DialView.didInteract(_:)))
        self.addGestureRecognizer(self.interactGesture)

        self.isUserInteractionEnabled = interactive
    }

    override func draw(_ rect: CGRect) {
        // dial empty full-circle
        let emptyPath = UIBezierPath(ovalIn: self.dialRect())
        emptyPath.lineWidth = self.emptyStrokeSize

        self.dialEmptyColor.setStroke()
        emptyPath.stroke()

        // dial background color
        self.dialBackgroundColor.setFill()
        emptyPath.fill()

        // dial filled semi-circle
        let arcEnd = self.dialFillAngle(progress: self.progress)
        let fillPath = UIBezierPath(arcCenter: self.dialCentroid(), radius: self.dialRadius(), startAngle: self.dialStartAngle(), endAngle: arcEnd, clockwise: true)

        fillPath.lineWidth = self.fillStrokeSize
        fillPath.lineCapStyle = .round

        self.dialFillColor.setStroke()
        fillPath.stroke()
    }

    // MARK: - Interaction
    private var nubInitialCenter = CGPoint()  // The initial center point of the view.
    private var progressDelta = Double(0)

    @objc func didInteract(_ gestureRecognizer : UIPanGestureRecognizer) {
        if gestureRecognizer.state == .began {
            self.nubInitialCenter = self.nubView.center
        }

        if gestureRecognizer.state == .changed {
            let c = dialCentroid()
            let p = gestureRecognizer.translation(in: self)
            let v = CGPoint(x: p.x + nubInitialCenter.x - c.x, y: p.y + nubInitialCenter.y - c.y)

            self.progress = vectorToProgress(v)
            self.delegate?.didChangeProgress(self)
        }

        if gestureRecognizer.state == .ended {

        }

    }

    private func vectorToProgress(_ vector: CGPoint) -> Double {
        let angle = atan2(vector.y, vector.x)
        var progress = Double((angle - dialStartAngle()) / (2 * .pi))

        if progress < 0.001 {
            progress = progress + 1
        }

//        let lastProgress = self.progress
//        let progressDelta = progress - lastProgress
//
//        if progressDelta > 0 && progress < lastProgress {
//            progress = 1.0
//        }
//
//        else if progressDelta < 0 && progress > lastProgress {
//            progress = 0.0
//        }

        return progress
    }

    private func angleToProgress(_ angle: CGFloat) {

    }

}

// MARK: - Draw Configuration
extension DialView {
    private func dialRect() -> CGRect {
        let inset = self.dialInset()
        var rect = CGRect(origin: CGPoint.zero, size: self.frame.size)
        rect = UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset))

        return rect
    }

    private func dialInset() -> CGFloat {
        return CGFloat(self.fillStrokeSize/2 + 11)
    }

    private func dialStartAngle() -> CGFloat {
        return CGFloat(.pi * -0.5)
    }

    private func dialFillAngle(progress: Double) -> CGFloat {
        return CGFloat(2 * .pi * progress) + self.dialStartAngle()
    }

    private func dialRadius() -> CGFloat {
        return self.dialRect().size.width * 0.5
    }

    private func dialCentroid() -> CGPoint {
        let radius = self.dialRadius()
        let inset = self.dialInset()

        return CGPoint(x: inset + radius, y: inset + radius)
    }

    private func nubRadius() -> CGFloat {
        return CGFloat(22.0)
    }

    private func nubSize() -> CGSize {
        let nr = nubRadius()
        return CGSize(width: nr * 2, height: nr * 2)
    }

    private func nubOrigin() -> CGPoint {
        let angle = dialFillAngle(progress: progress) + .pi/2

        // origin of the dial
        let o = self.dialCentroid()
        // point above center of dial
        let p = CGPoint(x: o.x, y: o.y - self.dialRadius())

        // rotate p about o `fill angle` degrees
        var nubOrigin = CGPoint(
            x: cos(angle) * (p.x - o.x) - sin(angle) * (p.y - o.y) + o.x,
            y: sin(angle) * (p.x - o.x) + cos(angle) * (p.y - o.y) + o.y)

        // adjust origin for nub frame center
        nubOrigin.x = nubOrigin.x - nubRadius()
        nubOrigin.y = nubOrigin.y - nubRadius()

        return nubOrigin
    }

}

class NubView: UIView {
    
    var outlineColor = UIColor.black
    
    var fillColor = UIColor.white
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        self.isOpaque = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        let strokeWidth = CGFloat(1.0)
        let padding = CGFloat(8.0)
        let inset = strokeWidth + padding
        let strokeInsets = UIEdgeInsetsMake(inset, inset, inset, inset)
        let nubRect = UIEdgeInsetsInsetRect(rect, strokeInsets)
        
        let nubPath = UIBezierPath(ovalIn: nubRect)
        
        nubPath.lineWidth = strokeWidth
        outlineColor.setStroke()
        
        fillColor.setFill()
        
        nubPath.fill()
        nubPath.stroke()
        
    }
}


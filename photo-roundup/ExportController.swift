//
//  ExportController.swift
//  photo-roundup
//
//  Created by Lee Irvine on 2/5/20.
//  Copyright © 2020 kezzi.co. All rights reserved.
//

import UIKit

protocol ExportControllerDelegate: NSObjectProtocol {
    
    // single file failed to export
    func exportDidFail(index: Int, _ message: String)
    
    // called repeatedly to report file system progress. 1.0 progress value for complete files.
    func updateExportStatus(index: Int, _ progress: Double)
    
    // all files moved
    func didCompleteExport()

    func nameForPhotoExport(index: Int) -> String
    
    func dataForPhotoExport(index: Int, _ completion: @escaping (Data?) -> ())
}


class ExportController: NSObject {
    
    var destination: URL!
    
    var delegate: ExportControllerDelegate!

//    private var queue: [Data] = [:]
    
    private var timer: DispatchSourceTimer?
            
//    func copyItem(at: URL, to: URL, _ completion: @escaping (Bool)->()) {
//        var success = false
//
//        DispatchQueue.global(qos: .background).async {
//            defer {
//                completion(success)
//            }
//
//            do {
//                try FileManager.default.copyItem(at: at, to: to)
//                success = true
//            } catch {
//                success = false
//            }
//        }
//    }
    
    private func reportProgress() {
        DispatchQueue.main.async {
            self.delegate.updateExportStatus(index: 0, 0.5)
        }

    }
    
    func stop() {
        if let timer = self.timer {
            timer.cancel()
        }
    }
    
    private func write(_ index: Int, _ completion: @escaping ()->()) {
        let writeQueue = DispatchQueue(label: "co.kezzi.progress.write")

        print("writing photo \(index)")
        
        let filename = self.delegate.nameForPhotoExport(index: index)
        
        self.delegate.dataForPhotoExport(index: index) { dataOrNil in
            writeQueue.async {
                do {
                    guard let data = dataOrNil else {
                        completion()
                        
                        return
                    }
                    
                    let writepath = self.destination.appendingPathComponent(filename).appendingPathExtension(".jpg")
                    
                    try data.write(to: writepath)

                    completion()

                    print("write photo \(index) pass \(filename)")

                    self.delegate.updateExportStatus(index: index, 1.0)
                    
                } catch {
                    print("write photo \(index) fail")
                    
                    self.delegate.exportDidFail(index: index, "fail to write")
                    
                    completion()
                }
            }
        }
        
    }
    
    
    func start(numberOfItems: Int) {
        let progressQueue = DispatchQueue(label: "co.kezzi.export.progress")
        
        if let oldTimer = self.timer {
            oldTimer.cancel()
        }
        
        let timer = DispatchSource.makeTimerSource(queue: progressQueue)

        timer.schedule(deadline: .now(), repeating: .seconds(1))

        timer.setEventHandler(handler: self.reportProgress)

        self.timer = timer
        
        timer.resume()

        
        let exportQueue = DispatchQueue(label: "co.kezzi.export")

        exportQueue.async {
            let groupSize:Int = 3
            
            (0..<(numberOfItems/groupSize)).forEach { groupIndex in
                let g = DispatchGroup()

                let simultaneousWrites = 3
                
                (0..<simultaneousWrites).forEach { i in
                    let index = groupIndex * groupSize + i
                    
                    print("[dispatch] index: \(index) group: \(groupIndex) enter \(i)")
                    g.enter()

                    self.write(index) {
                        print("[dispatch] index: \(index) group: \(groupIndex) leave \(i)")
                        g.leave()
                    }
                }
                
                // wait for simultaneous writes to complete on export queue
                g.wait()
                print("[dispatch] group finished \(groupIndex) -----------------")
            }
            
            DispatchQueue.main.async {
                 self.delegate.didCompleteExport()
            }

        }
    }
}




//
//  ExportViewController.swift
//  photo-roundup
//
//  Created by Lee Irvine on 8/13/18.
//  Copyright © 2018 kezzi.co. All rights reserved.
//

import UIKit
import MobileCoreServices // <------
import Photos // <-----

class ExportViewController: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var loadingView: LoadingView!
    
    @IBOutlet weak var exportButton: UIButton!
    
    var exporter: ExportController = ExportController()
    
    var photoController: PhotoController = PhotoController()
    
    var collectionVc: PhotoCollectionViewController!
    
    var indicesToExport:[Int] = []
    
    var successIndices:[Int] = []
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PhotoCollectionViewController {
            
            vc.photoController = self.photoController
            
            self.collectionVc = vc
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

//        self.loadingView.loading = true
        
        self.messageLabel.text = "Reading photo library"
        
        self.exportButton.isHidden = true
        
        self.exportButton.addTarget(self, action: #selector(didTapExportButton(_:)), for: .touchUpInside)
        
        self.photoController.fetch {
            self.messageLabel.text = "Found \(self.photoController.numberOfPhotos()) photos"
            
            self.collectionVc.collectionView.reloadData()
            
            self.exportButton.isHidden = false
            
            self.indicesToExport = (0..<self.photoController.numberOfPhotos()).map { $0 }
        }
    }

    @objc func didTapExportButton(_ sender: Any) {
        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypeFolder as String], in: .open)
        
        documentPicker.delegate = self
        
        self.present(documentPicker, animated: false)
    }
    
    func export(destination: URL, _ completion: @escaping ()->()) {

        self.exporter.destination = destination
        
//        let indicesToExport = self.indicesToExport.filter { self.successIndices.contains($0) == false }.count

        self.exporter.delegate = self

        self.exporter.start(numberOfItems: self.indicesToExport.count)
    }

        //        self.photoController.fetch {
                    
        //            let count = assets.count
                    
                    // TODO:
                    // get image meta data to name output files
                    // remove source image option (move)
                    // display # of images moved
                    // pause / resume operation when app is in background
                    // keep app awake during copy process
                    
        //            for i in (0..<assets.count) {
        //            for i in (0..<10) {
                        // get hydrade dial wheel
        //                let asset = assets[i]
        //                asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions()) { (editInput, info) in
        //                    guard let input = editInput, let imgURL = input.fullSizeImageURL else {
        //                        self.alert("!", message: "unable to get full size image URL for image")
        //
        //                        return
        //                    }
                            
        //                    do {
        //                        let toUrl = destination.appendingPathComponent("\(UUID().uuidString).jpg")
        //                        try FileManager.default.copyItem(at: imgURL, to: toUrl)
        //
        //                        self.alert("success!", message: imgURL.absoluteString)
        //                    } catch {
        //                        self.alert("!", message: "unable to write image to destination")
        //                    }
        //                }
        //            }
        //        }
        //        destination.stopAccessingSecurityScopedResource()
        //    }
}

extension ExportViewController : UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let url = urls.first else {
            self.alert("!", message: "no destination url provided.")
            return
        }
                
        guard url.startAccessingSecurityScopedResource() else {
            self.alert("!", message: "permission to access security scoped resource denied.")
            return
        }
        
        self.export(destination: url) {
            url.stopAccessingSecurityScopedResource()
        }
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
    }
}

extension ExportViewController : ExportControllerDelegate {
    func exportDidFail(index: Int, _ message: String) {
        
    }
    
    func updateExportStatus(index: Int, _ progress: Double) {
        
    }
    
    func didCompleteExport() {
        
    }
        
    func dataForPhotoExport(index: Int, _ completion: @escaping (Data?) -> ()) {
        let photoIndex = self.indicesToExport[index]
        print("[dataforphotoexport] call .image \(index)")

        self.photoController.data(index: photoIndex, size: CGSize(width: 1280, height: 800)) { data in
            print("[dataforphotoexport] complete .image \(index)")
            
            completion(data)
        }
    }
    
    func nameForPhotoExport(index: Int) -> String {
        let photoIndex = self.indicesToExport[index]

        guard let date = self.photoController.date(index: photoIndex) else { return UUID().uuidString }

        let filename = date.iso8601DateAndTime.replacingOccurrences(of: ":", with: "_")
        
        print("filename for \(photoIndex), \(filename)")
        
        return filename
    }
    
}

//
//  PhotoCell.swift
//  photo-roundup
//
//  Created by Lee Irvine on 8/13/18.
//  Copyright © 2018 kezzi.co. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var spinner: DialView!

    static var id: Int = 0
    var id: Int = 0

    class func nextId() -> Int {
        id = id + 1
        return id
    }

    func loading(_ isLoading: Bool, complete: Double) {
        self.spinner.isHidden = isLoading == false

        self.spinner.progress = complete
    }
}

//
//  PhotoCollectionViewController.swift
//  photo-roundup
//
//  Created by Lee Irvine on 2/4/20.
//  Copyright © 2020 kezzi.co. All rights reserved.
//

import UIKit

class PhotoCollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    var photoController: PhotoController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // set margin, height and width of cell
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout

        let size = UIScreen.main.bounds.size.width / 3
        
        layout.itemSize = CGSize(width: size, height: size)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
    }

    private func thumbnailSize() -> CGSize {
        let scale = UIScreen.main.scale

        let size = UIScreen.main.bounds.size.width / 3

        return CGSize(width: size * scale, height: size * scale)
    }

}

extension PhotoCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photoController.numberOfPhotos()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "image-cell", for: indexPath) as! PhotoCell
        
        cell.id = PhotoCell.nextId()
        
        let id:Int = cell.id

        cell.loading(true, complete: 0.5)
        
//        self.photoController.progress(index: indexPath.row) {
//
//        }

        self.photoController.image(index: indexPath.row, size: self.thumbnailSize()) { image in
            guard cell.id == id else {
                return
            }

            cell.imageView.image = image
//            cell.loading(false)
        }

        return cell
    }
}

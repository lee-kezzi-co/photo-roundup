//
//  PhotoController.swift
//  photo-roundup
//
//  Created by Lee Irvine on 8/13/18.
//  Copyright © 2018 kezzi.co. All rights reserved.
//

import UIKit
import Photos

protocol PhotoControllerDelegate: NSObjectProtocol {
    
}

class PhotoController: NSObject {

    private var imageAssets: PHFetchResult<PHAsset>?

    private var videoAssets: PHFetchResult<PHAsset>?

    private var delegate: PhotoControllerDelegate!

    convenience init(delegate: PhotoControllerDelegate) {
        self.init()

        self.delegate = delegate
    }

    // FIXME: fetch photo assets after permission to view photo library is granted
    
    // fetch all assets in camera roll
    func fetch(_ completion:@escaping ()->()) {
        DispatchQueue.global(qos: .userInitiated).async {
            let options = PHFetchOptions()
            options.includeAllBurstAssets = false
            options.includeHiddenAssets = false

            options.sortDescriptors = [ NSSortDescriptor(key: "creationDate", ascending: true) ]

            self.imageAssets = PHAsset.fetchAssets(with: .image, options: options)
            
            self.videoAssets = PHAsset.fetchAssets(with: .video, options: options)

            DispatchQueue.main.async {
                completion()
            }
        }

    }

    // get asset image for UI display purposes
    func image(index: Int, size: CGSize, completion: @escaping (UIImage) -> ()) {
        let asset = self.imageAssets![index]
        let options = PHImageRequestOptions()
        options.isSynchronous = false

        PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: .aspectFill, options: options) {
        (image, info) in
            if let image = image {
                completion(image)
            }
        }
    }

    // get asset image for UI display purposes
    func data(index: Int, size: CGSize, completion: @escaping (Data?) -> ()) {
        let asset = self.imageAssets![index]

        let options = PHImageRequestOptions()
        
        options.isSynchronous = true
        options.deliveryMode = .highQualityFormat
        options.isNetworkAccessAllowed = true

        let queue = DispatchQueue(label: "co.kezzi.photo.data")
        
        queue.async {
            PHImageManager.default().requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: options) {
            (image, info) in
                guard let image = image else { return }

                guard let data = UIImageJPEGRepresentation(image, 1.0) else {
                    print("failed to get image data from PHAsset")
                    
                    completion(nil)
                    
                    return
                }

                completion(data)
            }

        }


    }
    
    // get url of full size asset
    func url(index: Int, completion: @escaping (URL) -> ()) {
        guard let asset = self.imageAssets?[index] else { return }
        
        asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions()) { (editInput, info) in
            guard let input = editInput, let imgURL = input.fullSizeImageURL else { return }

            completion(imgURL)
        }
    }
    
    // get date and time photo was taken
    func date(index: Int) -> Date? {
        guard let asset = self.imageAssets?[index] else { return nil }

        return asset.creationDate
    }

    func numberOfPhotos() -> Int {
        return self.imageAssets?.count ?? 0
    }
}

//
//  String+Localize.swift
//  HTO
//
//  Created by Irvine, Lee on 4/11/19.
//  Copyright © 2019 Wizards of the Coast. All rights reserved.
//

import Foundation

extension String {
    
    func localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
    
    func paranthesized() -> String {
        return "(\(self))"
    }
    
}
